package com.accenture.mvpdemo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    private static final Logger logger = LogManager.getLogger(RestController.class);

    @GetMapping("/")
    public String greeting() {
        logger.info("Request Received to default endpoint!");
        return "Hello world!";
    }

    @PostMapping("/aws-log")
    public String awsLogs(@RequestBody String logs) {
        logger.info("Received the following logs from AWS: " + logs);
        return "Processed AWS Logs";
    }

    @GetMapping("/oops")
    public String error() {
        logger.error("Something went wrong!");
        return "Error!";
    }

}
